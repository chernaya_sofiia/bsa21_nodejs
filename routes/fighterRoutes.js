const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    const fighters = FighterService.getFighters();
    res.data = fighters;
    next();
}, responseMiddleware)

router.get('/:id', function (req, res, next) {
    const fighter = FighterService.search({ id: req.params.id });
    if (fighter) {
        res.data = fighter;
    }
    else {
        res.err = { status: 404, message: "Fighter not found" };
    }
    next();
}, responseMiddleware)

router.post('/', createFighterValid, function (req, res, next) {
    if (!res.err) {
        try {
            const fighter = FighterService.createFighter(req.body);
            if (fighter) {
                res.data = fighter;
            }
            else {
                res.err = { status: 404, message: "Fighter not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, message: err.toString() }
        }
    }
}, responseMiddleware)

router.put('/:id', updateFighterValid, function (req, res, next) {
    if (!res.err) {
        try {
            const fighter = FighterService.updateFighter(req.params.id, req.body);
            if (fighter) {
                res.data = fighter;
            }
            else {
                res.err = { status: 404, message: "Fighter not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, message: err.toString() }
        }
    }
    next();
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
    const fighter = FighterService.deleteFighter(req.params.id);
    if (fighter) {
        res.data = fighter;
    }
    else {
        res.err = { status: 404, message: "Fighter not found" };
    }
    next();
}, responseMiddleware)

module.exports = router;