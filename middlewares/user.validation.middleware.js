const { user } = require('../models/user');
const UserService = require('../services/userService');

function UserValid(data) {
    let result = { status: 200, message: "Everything ok" };
    for (let key in data) {
        if (key === "id") {
            result = { status: 400, message: "Field id cannot be in body of request" };
            break;
        }
        if (!user.hasOwnProperty(key)) {
            result = { status: 400, message: "User entity has isn't valid field" };
            break;
        }
        if (data[key].toString().length === 0) {
            result = { status: 400, message: "User`s fields cannot be empty" };
            break;
        }
        if (key === "phoneNumber") {
            if (data[key].length !== 13 || data[key].substr(0, 4) !== "+380") {
                result = { status: 400, message: "User phone number isn't valid" };
                break;
            }
        }
        if (key === "password") {
            if (typeof data[key] !== 'string') {
                result = { status: 400, message: "User password should be a string" };
                break;
            }
            if (data[key].length < 3) {
                result = { status: 400, message: "User password isn't valid" };
                break;
            }
        }
    };
    return result;
}

const createUserValid = (req, res, next) => {
    let err = UserValid(req.body);
    if (err) {
        res.err = err;
        res.err.status = 400;
    } else if (UserService.search({email: req.body.email})) {
        res.err = Error(`User entity to create isn't valid. User exist`);
        res.err.status = 400;
    }
    next();
}

const updateUserValid = (req, res, next) => {
    const data = req.body;
    if (Object.keys(data).length === 0) {
        res.err = { status: 400, message: "At least one user's field should be entered" };
        next();
    }
    const result = UserValid(data);
    if (result.status !== 200) {
        res.err = result;
        next();
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;