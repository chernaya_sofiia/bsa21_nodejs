const { fighter } = require('../models/fighter');

function FighterValid(data) {
    let result = { status: 200, message: "Everything ok" };
    for (const key in data) {
        if (key === "id") {
            result = { status: 400, message: "Field id cannot be in body of request" };
            break;
        }
        if (!fighter.hasOwnProperty(key)) {
            result = { status: 400, message: "Fighter entity has isn't valid field" };
            break;
        }
        if (data[key].toString().length === 0) {
            result = { status: 400, message: "Fighter`s fields cannot be empty" };
            break;
        }
        if (key === "power") {
            if (typeof data[key] !== "number") {
                result = { status: 400, message: "Fighter power should be a number" };
                break;
            }
            if (1 >= data[key] || data[key] >= 100) {
                result = { status: 400, message: "Fighter power isn't valid" };
                break;
            }
        }
        if (key === "defense") {
            if (typeof data[key] !== 'number') {
                result = { status: 400, message: "Fighter defense should be a number" };
                break;
            }
            if (1 >= data[key] || data[key] >= 10) {
                result = { status: 400, message: "Fighter defense isn't valid" };
                break;
            }
        }
        if (key === "health") {
            if (typeof data[key] !== 'number') {
                result = { status: 400, message: "Fighter health should be a number" };
                break;
            }
            if (80 >= data[key] || data[key] >= 120) {
                result = { status: 400, message: "Fighter health isn't valid" };
                break;
            }
        }
    };
    return result;
}

const createFighterValid = (req, res, next) => {
    let data = req.body;
    if (data.hasOwnProperty("health")) {
        if (Object.keys(data).length !== Object.keys(fighter).length - 1) {
            res.err = { status: 400, message: "Amount of fighters fields isn't valid" };
            next();
        }
    }
    else {
        if (Object.keys(data).length !== Object.keys(fighter).length - 2) {
            res.err = { status: 400, message: "Amount of fighters fields isn't valid" };
            next();
        }
    }
    const result = FighterValid(data);
    if (result.status !== 200) {
        res.err = result;
        next();
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    const data = req.body;
    if (Object.keys(data).length === 0) {
        res.err = { status: 400, message: "At least one fighter`s field should be fulled" };
        next();
    }
    const result = FighterValid(data);
    if (result.status !== 200) {
        res.err = result;
        next();
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;