const FightersService = require('../services/fightService');
const { fight } = require('../models/fight');

const createFightValid = (req, res, next) => {
    let err = FightValid(req.body, 'create');
    if (err) {
        res.err = err;
        res.err.status = 400;
    }
    next();
}

const updateFightValid = (req, res, next) => {
    let err = FightValid(req.body, 'update');
    if (err) {
        res.err = err;
        res.err.status = 400;
    } else if (!FightersService.search({ id: req.params.id })) {
        res.err = Error(`Fight entity to update isn't valid. Fight not exist`);
        res.err.status = 400;
    }
    next();
}

function FightValid(data) {
    let result = { status: 200, message: "Everything ok" };
    for (let key in data) {
        if (key === "id") {
            result = { status: 400, message: "Field id cannot be in body of request" };
            break;
        }
        if (!fight.hasOwnProperty(key)) {
            result = { status: 400, message: "Fighter entity has isn't valid field" };
            break;
        }
        if (key === "fighter1") {
            if (!FightersService.search({ fighter1: req.params.fighter1 })) {
                result = { status: 400, message: "Fight isn't valid. No 'fighter1' id" };
                break;
            }
        }
        if (key === "fighter2") {
            if (!FightersService.search({ fighter1: req.params.fighter2 })) {
                result = { status: 400, message: "Fight isn't valid. No 'fighter2' id" };
                break;
            }
        }
    }
    return result;
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;