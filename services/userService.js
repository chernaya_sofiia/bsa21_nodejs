const { UserRepository } = require('../repositories/userRepository');

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getUsers() {
        const users = UserRepository.getAll();
        return users;
    }

    createUser(user) {
        const usedEmail = this.search({ email: user.email });
        if (usedEmail) {
            throw Error("User with such email is already exists");
        }
        const usedPhoneNumber = this.search({ phoneNumber: user.phoneNumber });
        if (usedPhoneNumber) {
            throw Error("User with such phone number is already exists");
        }
        const createdUser = UserRepository.create(user);
        if (!createdUser) {
            throw Error('User not saved');
        }
        return createdUser;
    }

    updateUser(id, data) {
        if (!this.search({ id: id })) {
            return null;
        }
        if (data.email) {
            const usedEmail = this.search({ email: data.email });
            if (usedEmail) {
                throw Error("User with such email is already exists");
            }
        }
        if (data.phoneNumber) {
            const usedPhoneNumber = this.search({ phoneNumber: data.phoneNumber });
            if (usedPhoneNumber) {
                throw Error("User with such phoneNumber is already exists");
            }
        }
        const updatedUser = UserRepository.update(id, data);
        return updatedUser;
    }

    deleteUser(id) {
        if (!this.search({ id: id })) {
            return null;
        }
        const deletedUser = UserRepository.delete(id);
        return deletedUser;
    }
}

module.exports = new UserService();