const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    searchFighter(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getFighters() {
        const fighters = FighterRepository.getAll();
        return fighters;
    }

    createFighter(data) {
        if (this.checkFighterName(data.name)) {
            throw Error("Fighter with such name is already exists")
        }
        if (!data.health) {
            data.health = 100;
        }
        const item = FighterRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    createFighter(fighter) {
        const usedName = this.search({ name: fighter.name });
        if (usedName) {
            throw Error("Fighter with such name is already exists");
        }
        if (!data.health) {
            data.health = 100;
        }
        const createdFighter = FighterRepository.create(user);
        if (!createdFighter) {
            throw Error('User not saved');
        }
        return createdFighter;
    }

    updateFighter(id, data) {
        if (!this.searchFighter({ id: id })) {
            return null;
        }
        if (data.name) {
            const usedFighterName = this.search({ name: data.name });
            if (usedFighterName) {
                throw Error("Fighter with such name is already exists");
            }
        }
        const updatedFighter = FighterRepository.update(id, data);
        return updatedFighter;
    }

    deleteFighter(id) {
        if (!this.search({ id: id })) {
            return null;
        }
        const deletedFighter = FighterRepository.delete(id);
        return deletedFighter;
    }
}

module.exports = new FighterService();