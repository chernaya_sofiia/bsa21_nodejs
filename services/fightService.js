const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    search(search) {
        const item = FightRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getFights() {
        const fights = FightRepository.getAll();
        return fights;
    }

    getFight(data) {
        const fight = FightRepository.getOne(data);
        if (!fight) {
            throw Error('Fight not found');
        }
        return fight;
    }

    createFight(data) {
        const fightData = JSON.parse(data.log);
        const fight = FightRepository.create(fightData);
        if (!fight) {
            throw Error('Fight not saved');
        }
        return fight;
    }

    updateFight(id, data) {
        const fightData = JSON.parse(data.log);
        const fight = FightRepository.update(id, fightData);
        if (!fight) {
            throw Error('Fight not updated');
        }
        return fight;
    }

    deleteFight(id) {
        const fight = FightRepository.delete(id);
        if (!fight.length) {
            throw Error('Fight not deleted');
        }
        return fight;
    }
}

module.exports = new FightersService();